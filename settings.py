# -*- encoding: utf-8 -*-
from pathlib import Path

from django.urls import reverse_lazy

from wafer.settings import *

TIME_ZONE = 'America/Sao_Paulo'
USE_L10N = False
TIME_FORMAT = 'G:i'
SHORT_DATE_FORMAT = 'Y-m-d'
DATETIME_FORMAT = 'Y-m-d H:i:s'
SHORT_DATETIME_FORMAT = 'Y-m-d H:i'

try:
    from localsettings import *
except ImportError:
    pass

root = Path(__file__).parent

INSTALLED_APPS = (
    'dc19',
    'news',
    'django_countries',
    'paypal.standard.ipn',
) + INSTALLED_APPS

STATIC_ROOT = str(root / 'localstatic/')

STATICFILES_DIRS = (
    str(root / 'static'),
)

STATICFILES_STORAGE = (
    'django.contrib.staticfiles.storage.ManifestStaticFilesStorage')

TEMPLATES[0]['DIRS'] = TEMPLATES[0]['DIRS'] + (
    str(root / 'templates'),
)


WAFER_MENUS += (
    {
        'menu': 'about',
        'label': 'About',
        'items': [
            {
                'menu': 'debconf',
                'label': 'DebConf',
                'url': reverse_lazy('wafer_page', args=('about/debconf',)),
            },
            {
                'menu': 'debian',
                'label': 'Debian',
                'url': reverse_lazy('wafer_page', args=('about/debian',)),
            },
            {
                'menu': 'accommodation',
                'label': 'Accommodation',
                'url': reverse_lazy('wafer_page', args=('about/accommodation',)),
            },
            {
                'menu': 'bursaries',
                'label': 'Bursaries',
                'url': reverse_lazy('wafer_page', args=('about/bursaries',)),
            },
            {
                'menu': 'cheese-and-wine-party',
                'label': 'Cheese and Wine Party',
                'url': reverse_lazy('wafer_page', args=('about/cheese-and-wine-party',)),
            },
            {
                'menu': 'confdinner',
                'label': 'Conference Dinner',
                'url': reverse_lazy('wafer_page', args=('about/confdinner',)),
            },
            {
                'menu': 'curitiba',
                'label': 'Curitiba',
                'url': reverse_lazy('wafer_page', args=('about/curitiba',)),
            },
            {
                'menu': 'registration_information',
                'label': 'Registration Information',
                'url': reverse_lazy('wafer_page', args=('about/registration',)),
            },
            {
                'menu': 'venue',
                'label': 'Venue',
                'url': reverse_lazy('wafer_page', args=('about/venue',)),
            },
            {
                'menu': 'visas',
                'label': 'Visas',
                'url': reverse_lazy('wafer_page', args=('about/visas',)),
            },
        ],
    },
    {
        'menu': 'sponsors_index',
        'label': 'Sponsors',
        'items': [
            {
                'menu': 'become_sponsor',
                'label': 'Become a Sponsor',
                'url': reverse_lazy('wafer_page', args=('sponsors/become-a-sponsor',)),
            },
            {
                'menu': 'sponsors',
                'label': 'Our Sponsors',
                'url': reverse_lazy('wafer_sponsors'),
            },
        ],
    },
    {
        'menu': 'schedule_index',
        'label': 'Schedule',
        'items': [
            {
                'menu': 'cfp',
                'label': 'Call for Proposals',
                'url': reverse_lazy('wafer_page', args=('cfp',)),
            },
            {
                'menu': 'confirmed_talks',
                'label': 'Confirmed Talks',
                'url': reverse_lazy('wafer_users_talks'),
            },
            {
                'menu': 'important-dates',
                'label': 'Important Dates',
                'url': reverse_lazy('wafer_page', args=('schedule/important-dates',)),
            },
            {
                'menu': 'mobile_schedule',
                'label': 'Mobile-Friendly Schedule',
                'url': reverse_lazy('wafer_page', args=('schedule/mobile',)),
            },
            {
                'menu': 'openday',
                'label': 'Open Day',
                'url': reverse_lazy('wafer_page', args=('schedule/openday',)),
            },
            {
                'menu': 'schedule',
                'label': 'Schedule',
                'url': reverse_lazy('wafer_full_schedule'),
            },
        ],
    },
    {
        'menu': 'wiki_link',
        'label': 'Wiki',
        'url': 'https://wiki.debian.org/DebConf/19',
    },
    {
        'menu': 'sponsors_index',
        'label': 'Contact Us',
        'items': [
            {
                'menu': 'contact',
                'label': 'Contact Us',
                'url': reverse_lazy('wafer_page', args=('contact/contact',)),
            },
            {
                'menu': 'help',
                'label': 'Help us',
                'url': reverse_lazy('wafer_page', args=('contact/help',)),
            },
        ],
    },
)

WAFER_DYNAMIC_MENUS = ()

ROOT_URLCONF = 'urls'

CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_FAIL_SILENTLY = not DEBUG

MARKITUP_FILTER = ('markdown.markdown', {
    'extensions': [
        'markdown.extensions.smarty',
        'markdown.extensions.tables',
        'markdown.extensions.toc',
        'dc19.markdown',
    ],
    'output_format': 'html5',
    'safe_mode': False,
})
MARKITUP_SET = 'markitup/sets/markdown/'
JQUERY_URL = 'js/jquery.js'

DEFAULT_FROM_EMAIL = 'registration@debconf.org'
REGISTRATION_DEFAULT_FROM_EMAIL = 'noreply@debconf.org'

WAFER_REGISTRATION_MODE = 'custom'
#WAFER_USER_IS_REGISTERED = 'register.models.user_is_registered'

WAFER_PUBLIC_ATTENDEE_LIST = False

PAGE_DIR = '%s/' % (root / 'pages')
NEWS_DIR = '%s/' % (root / 'news' / 'stories')
SPONSORS_DIR = '%s/' % (root / 'sponsors')
