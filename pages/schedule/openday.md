---
name: Open Day
---
# Open Day

The Open Day (20 July) is targeted at the general public.
Events of interest to a wider audience will be offered, ranging from
topics specific to Debian to the greater Free Software community and maker
movement.

The event is a perfect opportunity for interested users to meet
our community, for us to broaden our community, and for our sponsors to
increase their visibility.

Less purely technical than main conference schedule, the events on Open
Day will cover a large range of topics from social and cultural issues
to workshops and introductions to Debian.

If you are interested in presenting at the open day, please read our
[Call For Participation](/cfp/) (once available), and submit your
proposal.
We also welcome suggestions of potential speakers to invite, please
e-mail <content@debconf.org>.

As with rest of DebConf, attendance is free of charge.
Although the Open Day is fairly relaxed, [registration][] in advance is
essential.

[registration]: /about/registration/

## When?

 * July 20, 2019 (Saturday).
 * 9 AM to 6 PM

## Where?

At [UTFPR Curitiba](/about/venue/).

## Job Fair

During the Open Day, we will have a Job Fair with booths from our
[sponsors](/sponsors/).

## FAQ

### Where do I submit my proposal?

We will open the [call for activities](/cfp/) in early 2019.
It will include instructions.

### What language are events in?

The Open Day will host events in multiple languages.
We expect to have events in English, Portuguese, and Spanish.

### Need help installing Debian?

We will host a Debian installfest, for attendees who would like to get help
installing Debian on their machine.

## Help out with Open Day!

DebConf runs on volunteer effort.
To get involved, see our [contact](/contact/contact/) page.
Open Day is being planned [on the
wiki](https://wiki.debian.org/DebConf/19/OpenDay/Planning).
