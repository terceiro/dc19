---
name: Important Dates
---

# Important Dates

| **JANUARY**           |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 15 (Tuesday)          | Opening of attendee registration and requesting bursaries            |


| **FEBRUARY**          |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 15 (Friday)           | Opening of Call For Proposals                                        |


| **MARCH**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 31 (Sunday)           | Last day for requesting bursarie                                     |


| **APRIL**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 30 (Tuesday)          | Bursarie decision                                                    |


| **JUNE**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 16 (Sunday)           | Last day for submitting a talk                                       |


| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| *DebCamp*             |                                                                      |
| 14 (Sunday)           | First day of DebCamp                                                 |
| 15 (Monday)           | Second day of DebCamp                                                |
| 16 (Tuesday)          | Third day of DebCamp                                                 |
| 17 (Wednesday)        | Fourth day of DebCamp                                                |
| 18 (Thursday)         | Fifth day of DebCamp                                                 |
| 19 (Friday)           | Sixth day of DebCamp                                                 |
| *OpenDay*             |                                                                      |
| 20 (Saturday)         | Open Day; afternoon: job fair; Arrival day for DebConf, Set-up       |
| *DebConf*             |                                                                      |
| 21 (Sunday)           | First day of DebConf / opening ceremony                              |
| 22 (Monday)           | Second day of DebConf / night: cheese and wine party                 |
| 23 (Tuesday)          | Third day of DebConf                                                 |
| 24 (Wednesday)        | Fourth day of DebConf / all day: day trip                            |
| 25 (Thursday)         | Fifth day of DebConf / night: conference dinner                      |
| 26 (Friday)           | Sixth day of DebConf                                                 |
| 27 (Saturday)         | Last day of DebConf / closing ceremony / teardown                    |
| 28 (Sunday)           | Free day / morning: visit handicraft fair / lunch brazilian barbecue |
| 29 (Monday)           | Departure day :-(                                                    |
